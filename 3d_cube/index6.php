<!-- CSS CUBE WITH ROTATION AND HOVER EFFECT -->
<!DOCTYPE html>
<html><head>
<style>

body {
  background-image: url("background.png"); /* Backgrouund image: pattern */
}


/* The view will containg the scene, it will allow us to position it easily whererver we want */
.view {
  position: absolute;
  width: 200px;
  height: 200px;
  top: 50%;
  left:50%;
  transform: translate(-50%,-50%);
  /* The perspective property defines how many pixels a 3D element is placed from the view. */
  /* When defining the perspective property for an element, it is the CHILD elements that get the perspective view, NOT the element itself. */
  /* Note: The perspective property only affects 3D transformed elements! */
  -webkit-perspective: 400px;
          perspective: 400px;
}


/* The Scene will be the animated part, we will construct objects inside and then animate them all together */
.scene {
  position: absolute;
  width: 200px;
  height: 200px;
  margin: auto;
  -webkit-transform-style: preserve-3d;
          transform-style: preserve-3d;
  -webkit-animation: rotate 20s infinite linear;
          animation: rotate 20s infinite linear;
}

/* We will put 3 squares inside the scene, 200px side. */
/* We will use this three squares to create the sides of the cube with ::after and ::before selectors */
.scene .square {

  width: 200px;
  height: 200px;
  position: absolute;
  -webkit-transform-style: preserve-3d;
          transform-style: preserve-3d;

}

/* In each one of the 3 squares we will create an "after" and a "before" element. */
/* Both of them have this code in commmon. They have an empty content, the size will be */
/* the parent's size, and the will have an inside shadow with a blur effect. */
/* We will also add the way they must transition for the hover effect. */
.scene .square::before, .scene .square::after {
  content: "";
  display: block;
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  margin: auto;
  width: 100%;
  height: 100%;
  /* border-radius --> Add rounded borders to a <div> element */
  border-radius: 0%;
  /* box-sizing --> Is used to tell the browser what the sizing properties (width and height) should include. */
  /*                + content-box: Default. The width and height properties (and min/max properties) includes only the content. Border, padding, or margin are not included. */
  /*                + border-box:: The width and height properties (and min/max properties) includes content, padding and border, but not the margin */
  box-sizing: border-box;
  /* box-shadow --> Attaches one or more shadows to an element. */
  /* box-shadow: h-shadow (posicion horizontal) v-shadow(posicion vertical) blur(blur) spread(tamaño de la sombra) color | inset(sombra interna) h-shadow v-shadow blur spread color; */
  box-shadow: inset 0 0 60px crimson;
  /* To create a transition effect, you must specify three things: */
  /*  + The CSS property you want to add an effect to. */
  /*  + The duration of the effect. */
  /*  + Speed Curve of the Transition (optional). */
  transition: transform 0.5s ease-out;
}

/* Each before element will be translated on Z axis -100 pixels.*/
/* This way we will have an square with an inside blur shadow at z = -100 px */
.scene .square::before {
  -webkit-transform: translateZ(-100px);
          transform: translateZ(-100px);
}

/* Each after element will be translated on Z axis +100 pixels.*/
/* This way we will have an square with an inside blur shadow at z = +100 px */
.scene .square::after {
  -webkit-transform: translateZ(100px);
          transform: translateZ(100px);
}

/* So, from the initial flat square, now we have to pararell squares on 3D perspective */
/* separeted from each other 200px on the Z axis. */
/* With the 3 pairs of squares we will construct the cube: */

/* First child remain as it is, becoming the front and the back sides of the cube. */
.scene .square:nth-child(1) {
  -webkit-transform: rotateZ(0deg) ;
          transform: rotateZ(0deg) ;
}
/* Second child is rotated on X axis 90 degres, becoming top and bottom sides of the cube. */
.scene .square:nth-child(2) {
  -webkit-transform: rotateX(90deg) ;
          transform: rotateX(90deg) ;
}
/* Third child is rotated on Y axis 90 degres, becoming left and right sides of the cube. */
.scene .square:nth-child(3) {
  -webkit-transform: rotateY(90deg);
          transform: rotateY(90deg);
}

/* Here is the animation we apply to the scene. Uniform rotation on three axis. */
@keyframes rotate {
  from {transform:  rotateY(0deg) rotateX(0deg) rotateZ(0deg) ;}
    to {transform:  rotateY(360deg) rotateX(360deg) rotateZ(360deg);}
  }

/* On hover over the scene we will separate the "before" and "after" square objects on */
/* the Z axis so the wffect will be the cube opening. */
.scene:hover div.square::before {transform: translateZ(-200px); }
.scene:hover div.square::after  {transform: translateZ(200px); }

button {
  position: absolute;
  bottom: 0;
  right: 0;
  margin: 15px;
}

button.left {
  position: absolute;
  left: 0;
  bottom: : 0;
  margin: 15px;
}

</style>
</head>


<body>
<div class="view"" id="view">
  <div class="scene">
    <div class="square"></div>
    <div class="square"></div>
    <div class="square"></div>
</div>
</div>
           
<button class="left" type="button"><a href="./index5.php">Anterior</a></button>
<button type="button"><a href="./index7.php">Siguiente</a></button>
</body></html>


