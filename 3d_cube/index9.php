<!DOCTYPE html><html class=''>
<head>


<style class="cp-pen-styles">.svg-pentagon {
            text-align: center;
        }
        .svg-pentagon svg {

        }
        .pentagon {
            stroke-width:0.5px
            -webkit-transform:rotate(36deg);
            -moz-transform:rotate(36deg);
            //transform:rotate(36deg);
        }
        .pentagon-1 {
            fill: #EFE3E5;
        }
        .pentagon-2 {
            fill: #F0D9D8;
        }
        .pentagon-3 {
            fill: #F1CDC5;
        }
        .pentagon-4 {
            fill: #F2C1B6;
        }
        .pentagon-5 {
            fill: #F3AB97;
            stroke-width: 1px;
            stroke: rgba(0, 0, 0, .5);
        }
</style></head><body>
<div class="svg-pentagon">
    <svg id="J-svg-pentagon"  width="250" height="250">
        <g transform="translate(88, 95)">
            <polygon class="pentagon pentagon-5" />
            <polygon class="pentagon pentagon-4" />
            <polygon class="pentagon pentagon-3" />
            <polygon class="pentagon pentagon-2" />
            <polygon class="pentagon pentagon-1" />

        </g>
    </svg>
</div>
<script src='//assets.codepen.io/assets/common/stopExecutionOnTimeout-53beeb1a007ec32040abaf4c9385ebfc.js'></script><script src='//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script>var svgPentagon = $('#J-svg-pentagon');
var points = {
    base: [
        '6',
        '1',
        '10.7552826',
        '4.45491503',
        '8.93892626',
        '10.045085',
        '3.06107374',
        '10.045085',
        '1.24471742',
        '4.45491503'
    ],
    pointsAbility: []
};
var setPentagon = function (num) {
    for (i = 1; i <= num; i++) {
        if (window.CP.shouldStopExecution(2)) {
            break;
        }
        var pointsChildren = 'points' + i;
        points[pointsChildren] = new Array();
        for (j = 0; j < points.base.length; j++) {
            if (window.CP.shouldStopExecution(1)) {
                break;
            }
            points[pointsChildren].push((parseFloat(points.base[j]) * (6 + i * 4)).toFixed(2));
            points[pointsChildren][j] = parseFloat(points[pointsChildren][j]) - i * 24;
        }
        window.CP.exitedLoop(1);
        svgPentagon.find('.pentagon-' + i).attr('points', points[pointsChildren].join(' '));
    }
    window.CP.exitedLoop(2);
};
setPentagon(5);
//# sourceURL=pen.js
</script>
</body></html>