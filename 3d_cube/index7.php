<!-- ICOSAEDRO -->
<!DOCTYPE html>
<html><head>
<style>

body {
  background-image: url("background.png"); /* Backgrouund image: pattern */
}


/* The view will containg the scene, it will allow us to position it easily whererver we want */
.view {
  position: absolute;
  width: 200px;
  height: 173.32px;
  top: 50%;
  left:50%;
  transform: translate(-50%,-50%);
  /* The perspective property defines how many pixels a 3D element is placed from the view. */
  /* When defining the perspective property for an element, it is the CHILD elements that get the perspective view, NOT the element itself. */
  /* Note: The perspective property only affects 3D transformed elements! */
  -webkit-perspective: 400px;
          perspective: 400px;
}


/* The Scene will be the animated part, we will construct objects inside and then animate them all together */
.scene {
  position: absolute;
  width: 200px;
  height: 173.32px;
  margin: auto;
  -webkit-transform-style: preserve-3d;
          transform-style: preserve-3d;
  -webkit-animation: rotate 20s infinite linear;
          animation: rotate 20s infinite linear;


          transform: rotateY(40deg);
}



.scene .face {

  width: 200px;
  height: 173.32px;
  position: absolute;
  -webkit-transform-style: preserve-3d;
          transform-style: preserve-3d;

}


.scene .face::before, .scene .face::after {
  content: url(triangle.svg);
  display: block;
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  margin: auto;
  width: 100%;
  height: 100%;
  transition: transform 0.5s ease-out;

}


.scene .face::before {

  -webkit-transform: translateY(70px) rotateX(35.22deg);
          transform: translateY(70px) rotateX(35.22deg);
}


.scene .face::after {


  -webkit-transform: translateY(-70px) rotateX(-35.22deg) rotateZ(180deg);
          transform: translateY(-70px) rotateX(-35.22deg) rotateZ(180deg);
}



.scene .face:nth-child(1) {
  -webkit-transform: translateZ(-50px);
          transform: translateZ(-50px);
}

.scene .face:nth-child(2) {
  -webkit-transform:  rotateY(90deg) translateZ(-50px);
          transform:  rotateY(90deg) translateZ(-50px);
}

.scene .face:nth-child(3) {
  -webkit-transform:  rotateY(180deg) translateZ(-50px);
          transform:  rotateY(180deg) translateZ(-50px);
}

.scene .face:nth-child(4) {
  -webkit-transform:  rotateY(270deg) translateZ(-50px);
          transform:  rotateY(270deg) translateZ(-50px);
}

@keyframes rotate {
  from {transform:  rotateY(0deg)  ;}
    to {transform:  rotateY(360deg) ;}
  }



.scene:hover div.face::before {transform: translateY( 150px) rotateX(35.22deg); }
.scene:hover div.face::after  {transform: translateY(-150px) rotateX(-35.22deg) rotateZ(180deg); }

button {
	position: absolute;
	bottom: 0;
	right: 0;
	margin: 15px;
}
button.left {
  position: absolute;
  left: 0;
  bottom: : 0;
  margin: 15px;
}
</style>
</head>


<body>
<div class="view"" id="view">
  <div class="scene">


<div class="face"></div>
<div class="face"></div>
<div class="face"></div>
<div class="face"></div>




</div>
</div>

<button class="left" type="button"><a href="./index6.php">Anterior</a></button>
<button type="button"><a href="./index8.php">Siguiente</a></button>
           

</body></html>


