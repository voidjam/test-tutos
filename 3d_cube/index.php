<!DOCTYPE html>
<html><head>
<style>

body {

	background-image: url("background.png");
}

.SCENE {
	-webkit-perspective: 600px;
	width: 600px;
	height: 340px;
	position: absolute;
	top: 50%;
  	left:50%;
	transform: translate(-50%,-50%);
	background-color: transparent;
}

.OBJECT {
	-webkit-transform-style: preserve-3d;
	-webkit-transform: rotateX(0deg) rotateY(0deg) rotateZ(0deg);
	transform-origin: 50% 50%;
	position: relative;
	width: 100%;
	height: 100%;

}

.FACE {
	position: absolute;
	display: inline-block;
	left: 200px;
	top: 70px;
	height: 200px;
	width: 200px;
}

.FACE img	{
	transition: transform 0.5s linear;
}

.FACE.im1 { -webkit-transform: translateZ(100px) ; }
.FACE.im2 { -webkit-transform: translateZ(-100px); }
.FACE.im3 { -webkit-transform: translateX(100px) rotateY(90deg);}
.FACE.im4 { -webkit-transform: translateX(-100px) rotateY(-90deg);}
.FACE.im5 { -webkit-transform: translateY(100px) rotateX(90deg);}
.FACE.im6 { -webkit-transform: translateY(-100px) rotateX(-90deg);}



.OBJECT:hover img.im1 {transform: translateZ(250px); }
.OBJECT:hover img.im2 {transform: translateZ(-250px); }
.OBJECT:hover img.im3 {transform: translateX(250px) rotateY(90deg);}
.OBJECT:hover img.im4 {transform: translateX(-250px) rotateY(-90deg);}
.OBJECT:hover img.im5 {transform: translateY(250px) rotateX(90deg);}
.OBJECT:hover img.im6 {transform: translateY(-250px) rotateX(-90deg);}



div#obj6 {

    animation: rotate3d7 linear infinite 10s;
    animation-name: rotate3d7;
    animation-duration: 10s;
    animation-timing-function: linear;
    animation-delay: initial;
    animation-iteration-count: infinite;
    animation-direction: initial;
    animation-fill-mode: initial;
    animation-play-state: initial;

}


/* Standard syntax */
@keyframes rotate3d7 {
    from {transform:  rotateY(0deg) rotateX(45deg) rotateZ(45deg) ;}
    to {transform:  rotateY(360deg) rotateX(45deg) rotateZ(45deg) ;}
}

div#obj6 img
	{transition: 0.5s linear;}

button {
	position: absolute;
	bottom: 0;
	right: 0;
	margin: 15px;
}

</style>
</head>


<body>
<div class="SCENE" style="margin-top: 0px" id="sce6">
           
           <div class="OBJECT" id="obj6">
             <img class="FACE im1" src="./square.png" id="im1">
             <img class="FACE im2" src="./square.png" id="im2">
             <img class="FACE im3" src="./square.png" id="im3">
             <img class="FACE im4" src="./square.png" id="im4">
             <img class="FACE im5" src="./square.png" id="im5">
             <img class="FACE im6" src="./square.png" id="im6">
           </div>
        </div>

<button type="button"><a href="./index1.php">Siguiente</a></button>
</body></html>


