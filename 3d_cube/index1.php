<!-- atomo -->
<!DOCTYPE html><html>
<head>

<style>

body {

	background-image: url("background.png");
}


/* CUBE */

.SCENE {
	-webkit-perspective: 600px;
	width: 600px;
	height: 340px;
	position: absolute;
	top: 50%;
  	left:50%;
	transform: translate(-50%,-50%);
	background-color: transparent;
}

.OBJECT {
	-webkit-transform-style: preserve-3d;
	-webkit-transform: rotateX(0deg) rotateY(0deg) rotateZ(0deg);
	transform-origin: 50% 50%;
	position: relative;
	width: 100%;
	height: 100%;

}

.FACE {
	position: absolute;
	display: inline-block;
	left: 200px;
	top: 70px;
	height: 200px;
	width: 200px;
}

.FACE img	{
	transition: transform 0.5s linear;
}

.FACE.im1 { -webkit-transform: translateZ(100px) ; }
.FACE.im2 { -webkit-transform: translateZ(-100px); }
.FACE.im3 { -webkit-transform: translateX(100px) rotateY(90deg);}
.FACE.im4 { -webkit-transform: translateX(-100px) rotateY(-90deg);}
.FACE.im5 { -webkit-transform: translateY(100px) rotateX(90deg);}
.FACE.im6 { -webkit-transform: translateY(-100px) rotateX(-90deg);}



.SCENE:hover img.im1 {transform: translateZ(250px); }
.SCENE:hover img.im2 {transform: translateZ(-250px); }
.SCENE:hover img.im3 {transform: translateX(250px) rotateY(90deg);}
.SCENE:hover img.im4 {transform: translateX(-250px) rotateY(-90deg);}
.SCENE:hover img.im5 {transform: translateY(250px) rotateX(90deg);}
.SCENE:hover img.im6 {transform: translateY(-250px) rotateX(-90deg);}



div#obj6 {

    animation: rotate3d7 linear infinite 10s;
    animation-name: rotate3d7;
    animation-duration: 10s;
    animation-timing-function: linear;
    animation-delay: initial;
    animation-iteration-count: infinite;
    animation-direction: initial;
    animation-fill-mode: initial;
    animation-play-state: initial;

}


/* Standard syntax */
@keyframes rotate3d7 {
    from {transform:  rotateY(0deg) rotateX(45deg) rotateZ(45deg) ;}
    to {transform:  rotateY(360deg) rotateX(45deg) rotateZ(45deg) ;}
}

div#obj6 img
	{transition: 0.5s linear;}





/* SPHERE */

.view {
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  -webkit-perspective: 400;
          perspective: 400;
  -webkit-transform-style: preserve-3d;
          transform-style: preserve-3d;

}

.plane {
  width: 120px;
  height: 120px;
  -webkit-transform-style: preserve-3d;
          transform-style: preserve-3d;
}
.plane.main {
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  margin: auto;
  -webkit-transform: rotateX(60deg) rotateZ(-30deg);
          transform: rotateX(60deg) rotateZ(-30deg);
  -webkit-animation: rotate 20s infinite linear;
          animation: rotate 20s infinite linear;
}
.plane.main .circle {
  width: 120px;
  height: 120px;
  position: absolute;
  -webkit-transform-style: preserve-3d;
          transform-style: preserve-3d;
  border-radius: 100%;
  box-sizing: border-box;
  box-shadow: 0 0 60px crimson, inset 0 0 60px crimson;
}
.plane.main .circle::before, .plane.main .circle::after {
  content: "";
  display: block;
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  margin: auto;
  width: 10%;
  height: 10%;
  border-radius: 100%;
  background: crimson;
  box-sizing: border-box;
  box-shadow: 0 0 60px 2px crimson;
}
.plane.main .circle::before {
  -webkit-transform: translateZ(-90px);
          transform: translateZ(-90px);
}
.plane.main .circle::after {
  -webkit-transform: translateZ(90px);
          transform: translateZ(90px);
}
.plane.main .circle:nth-child(1) {
  -webkit-transform: rotateZ(72deg) rotateX(63.435deg);
          transform: rotateZ(72deg) rotateX(63.435deg);
}
.plane.main .circle:nth-child(2) {
  -webkit-transform: rotateZ(144deg) rotateX(63.435deg);
          transform: rotateZ(144deg) rotateX(63.435deg);
}
.plane.main .circle:nth-child(3) {
  -webkit-transform: rotateZ(216deg) rotateX(63.435deg);
          transform: rotateZ(216deg) rotateX(63.435deg);
}
.plane.main .circle:nth-child(4) {
  -webkit-transform: rotateZ(288deg) rotateX(63.435deg);
          transform: rotateZ(288deg) rotateX(63.435deg);
}
.plane.main .circle:nth-child(5) {
  -webkit-transform: rotateZ(360deg) rotateX(63.435deg);
          transform: rotateZ(360deg) rotateX(63.435deg);
}

@-webkit-keyframes rotate {
  0% {
    -webkit-transform: rotateX(0) rotateY(0) rotateZ(0);
            transform: rotateX(0) rotateY(0) rotateZ(0);
  }
  100% {
    -webkit-transform: rotateX(360deg) rotateY(360deg) rotateZ(360deg);
            transform: rotateX(360deg) rotateY(360deg) rotateZ(360deg);
  }
}

@keyframes rotate {
  0% {
    -webkit-transform: rotateX(0) rotateY(0) rotateZ(0);
            transform: rotateX(0) rotateY(0) rotateZ(0);
  }
  100% {
    -webkit-transform: rotateX(360deg) rotateY(360deg) rotateZ(360deg);
            transform: rotateX(360deg) rotateY(360deg) rotateZ(360deg);
  }
}


button {
  position: absolute;
  bottom: 0;
  right: 0;
  margin: 15px;
}

button.left {
  position: absolute;
  left: 0;
  bottom: : 0;
  margin: 15px;
}

</style>
</head>


<body>
<div class="SCENE" style="margin-top: 0px" id="sce6">


	

	<!-- CUBE -->
	<div class="OBJECT" id="obj6">
		<img class="FACE im1" src="./square.png" id="im1">
		<img class="FACE im2" src="./square.png" id="im2">
		<img class="FACE im3" src="./square.png" id="im3">

		<!-- SPEHRE -->
		<div class="view">
		<div class="plane main">
			<div class="circle"></div>
			<div class="circle"></div>
			<div class="circle"></div>
			<div class="circle"></div>
			<div class="circle"></div>
			<div class="circle"></div>
		</div>
		</div>


		<img class="FACE im4" src="./square.png" id="im4">
		<img class="FACE im5" src="./square.png" id="im5">
		<img class="FACE im6" src="./square.png" id="im6">
	</div>
</div>
<button class="left" type="button"><a href="./index.php">Anterior</a></button>
<button type="button"><a href="./index2.php">Siguiente</a></button>
</body>


