jQuery(document).ready(function($){
	
	var gallery = $('.cd-gallery'),
		foldingPanel = $('.cd-folding-panel'),
		mainContent = $('.cd-main');
	
	/* On click on any .cd-item a element open folding content */
	gallery.on('click', 'a', function(event){
		event.preventDefault();
		openItemInfo($(this).attr('href'));
	});

	/* On click on close button close folding content */
	foldingPanel.on('click', '.cd-close', function(event){
		event.preventDefault();
		toggleContent('', false);
	});

	/* Detect click on .cd-gallery::before when the .cd-folding-panel is open */
	gallery.on('click', function(event){
		if($(event.target).is('.cd-gallery') && $('.fold-is-open').length > 0 ) toggleContent('', false);
	})

	function openItemInfo(url) {
		var mq = viewportSize();
		/* If content is visible above the .cd-gallery - scroll before opening the folding panel */
		if( gallery.offset().top > $(window).scrollTop() && mq != 'mobile') {
			$('body,html').animate({
				'scrollTop': gallery.offset().top
			}, 100, function(){ 
	           	toggleContent(url, true);
	        }); 

	    /* If content is visible below the .cd-gallery - scroll before opening the folding panel */
	    } else if( gallery.offset().top + gallery.height() < $(window).scrollTop() + $(window).height()  && mq != 'mobile' ) {
			$('body,html').animate({
				'scrollTop': gallery.offset().top + gallery.height() - $(window).height()
			}, 100, function(){ 
	           	toggleContent(url, true);
	        });
	        
		} else {
			toggleContent(url, true);
		}
	}

	/*
     * This functon open or closes the folding panel. In ther firs case
     * it will load the content given by url parameter.
     * 
     * @param url specifies the content file to load.
     * @param bool weather to open & load or close the folding panel.
     */
	function toggleContent(url, bool) {
		if( bool ) {
			/* load and show new content */
			var foldingContent = foldingPanel.find('.cd-fold-content');
			foldingContent.load(url+' .cd-fold-content > *', function(event){
				setTimeout(function(){
					$('body').addClass('overflow-hidden');
					foldingPanel.addClass('is-open');
					mainContent.addClass('fold-is-open');
				}, 100);
				
			});

		/* Close the folding panel */	
		} else {
			
			var mq = viewportSize();
			foldingPanel.removeClass('is-open');
			mainContent.removeClass('fold-is-open');
			
			/* According to the mq, immediately remove the .overflow-hidden or wait for the end of the animation.
			   On Desktop version each cd-item is translated separately. On Mobile version we translate the whole
			   cd-main at once. */
			(mq == 'mobile' || $('.no-csstransitions').length > 0 ) /* no-csstramsition is a modernizr.js class */
				
				? $('body').removeClass('overflow-hidden')
				
				: mainContent.find('.cd-item').eq(0).one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(){
					$('body').removeClass('overflow-hidden');
					mainContent.find('.cd-item').eq(0).off('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend');
				});
		}
		
	}

	// Returns the content of the .cd-main:before pseudo element so we know if viewport min-width is less 
    // of higher than 600px, that is .cd-main:before.content = mobile or .cd-main:before = desktop.
	function viewportSize() {
		/* retrieve the content value of .cd-main::before to check the actua mq */
		return window.getComputedStyle(document.querySelector('.cd-main'), '::before').getPropertyValue('content').replace(/"/g, "").replace(/'/g, "");
	}
});